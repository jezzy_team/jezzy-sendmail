<?php

class User {

    public $htmlDesigne;
    private $filePath;

    function __construct($action, $post) {

        $this->filePath = str_replace("api.php", "", $_SERVER["SCRIPT_FILENAME"]);

        switch ($action) {
            case "wellcome":
                $this->htmlDesigne = $this->wellcomeEmail($post);
                break;
            case "inactive":
                $this->htmlDesigne = $this->inactiveEmail();
                break;
            case "thanks":
                $this->htmlDesigne = $this->thanksEmail();
                break;
            case "endt":
                $this->htmlDesigne = $this->endTransactionEmail();
                break;
            case "or":
                $this->htmlDesigne = $this->orderReceivedEmail();
                break;
            case "ap":
                $this->htmlDesigne = $this->aprovedPaymentEmail();
                break;
            default:
                break;
        }
    }

    private function wellcomeEmail($paransArr) {
        $html = file_get_contents($this->filePath . "Layouts/UserWellcome.html");
        foreach ($paransArr as $key => $value) {
            $html = str_replace("{[(" . $key . ")]}", $value, $html);
        }
        return $html;
    }

    private function inactiveEmail() {
        return file_get_contents($this->filePath . "Layouts/UserInactive.html");
    }

    private function thanksEmail() {
        return file_get_contents($this->filePath . "Layouts/UserThanks.html");
    }

    private function endTransactionEmail() {
        return file_get_contents($this->filePath . "Layouts/UserEndTransaction.html");
    }

    private function orderReceivedEmail() {
        return file_get_contents($this->filePath . "Layouts/UserOrderReceived.html");
    }

    private function aprovedPaymentEmail() {
        return file_get_contents($this->filePath . "Layouts/UserAprovedPayment.html");
    }

}
