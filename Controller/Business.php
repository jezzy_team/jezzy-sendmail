<?php

class Business {

    public $htmlDesigne;
    private $filePath;

    function __construct($action) {

        $this->filePath = str_replace("api.php", "", $_SERVER["SCRIPT_FILENAME"]);

        switch ($action) {
            case "wellcome":
                $this->htmlDesigne = $this->wellcomeEmail();
                break;
            case "inactive":
                $this->htmlDesigne = $this->inactiveEmail();
                break;
            default:
                break;
        }
    }

    private function wellcomeEmail() {
        return "OIEEEEE";
    }

    private function inactiveEmail() {
        return file_get_contents($this->filePath . "Layouts/inactive.html");
    }

}
