<?php

//
// Super simples exemplo PHP que envia HTTP POST para um site remoto
//
function sendRequest($url, $parans) {
    $ch = curl_init ();
    curl_setopt ( $ch, CURLOPT_URL, $url );
    curl_setopt ( $ch, CURLOPT_POST, 1 );
    curl_setopt ( $ch, CURLOPT_POSTFIELDS, $parans );
    curl_setopt ( $ch, CURLOPT_RETURNTRANSFER, true );
    $server_output = curl_exec ( $ch );
    curl_close ( $ch );
    return $server_output;
}
$url = "http://localhost/accential/jezzy-sendmail/api.php?function=user&action=wellcome";
 
//BEGIN TESTE BLOCK
$parans  = http_build_query(array('name' => 'Leandro Lopes', 'email' => 'leandrolt+2@gmail.com', 'pass' => 'suaSenha'));
$return = sendRequest ( $url, $parans );
echo($return);
//END TESTE BLOCK