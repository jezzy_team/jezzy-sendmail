<?php

if(isset($_GET["function"]) && !empty($_GET["function"])){
    switch ($_GET["function"]){
        case "user":
            require_once 'Controller/User.php';
            $emailReturn = new User($_GET["action"], $_POST);
            break;
        case "company":
            require_once 'Controller/Business.php';
            $emailReturn = new Business($_GET["action"]);
            break;
    }
    echo $emailReturn->htmlDesigne ;
}
